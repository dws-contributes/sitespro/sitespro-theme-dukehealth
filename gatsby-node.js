exports.onPreInit = ({ reporter }) => {
  try {
    require(`@dws-contributes/sitespro-gatsby-base`)
  } catch {
    reporter.error(
      `You do not appear to have \`sitespro-gatsby-base\` installed.\n \`sitespro-theme-dukehealth\` is designed as an add-on to\n \`sitespro-gatsby-base\` and will not work without it.`
    )
  }
}