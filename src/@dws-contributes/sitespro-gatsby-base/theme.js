import deepmerge from 'deepmerge'
import baseTheme from "@dws-contributes/sitespro-gatsby-base/src/theme"

const customTheme = {
  footerBackground: '',

  cssVariables: {
    dark: {
      color: {
        primary: '#00539B', // health blue
        secondary: '#407E99', // light Blue 02
        accentOne: '#FFD960', // dandelion

        header: {
          primary: '#00539B', // DH blue
          secondary: '#ffffff', // white
          divider: '#ECECEC', // table row dark
          logo: '#ffffff', // white
          search: {
            text: '#464343', // text gray
            background: '#E5E5E5', // limestone
            icon:'#00539B', // health blue
          }
        }, 

        footer: {
          primary: '#212121', // DH black
          logo: '#ffffff', // white
          text: '#ffffff', // white
          divider: '#212121', // DH black     
          social: '#00539B', // health blue
        },

        button: {
          primary: {
            text: '#547533', // green 01
            background: '#ffffff', // white
            border: '#5E802C', // green 02
            textHover: '#ffffff', // white
            backgroundHover: '#5E802C', // green 02
            borderHover: '#5E802C', // green 02
          },
          cta: {
            text: '#262626', // cast iron
            background: '#E0750B', // orange 02
            border: '#E0750B', // orange 02
            textHover: '#262626', // cast iron
            backgroundHover: '#E79A5E', // orange 03
            borderHover: '#E79A5E', // orange 03
          },
          footer: {
            text: '#ffffff', // white
            background: '#00539B', // health blue
            border: '#00539B', // health blue
            textHover: '#ffffff', // white
            backgroundHover: '#056CA1', // accessible shale blue
            borderHover: '#056CA1', // accessible shale blue
          },                
        }, 

        menu: {
          main: {
            linkHover: '#00539B', // health blue
            linkActive: '#FFD960', // dandelion
            linkActiveHover: '#00539B', // health blue
            backgroundHover: '#ffffff', // white
            dropdownLink: '#00539B', // health blue
            linkMobile: '#00539B', // health blue
            dropdownLinkMobile: '#00539B', // health blue
          },
          utility: {
            link: '#ffffff', // white
            linkMobile: '#00539B', // health blue
          },
        }         
      }
    }  
  }
}

const theme = deepmerge(baseTheme, customTheme)
export default theme
