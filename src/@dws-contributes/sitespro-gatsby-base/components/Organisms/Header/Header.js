import React, { useState } from "react"
import PropTypes from "prop-types"
import bem from "@dws-contributes/sitespro-gatsby-base/src/components/_utils/bem"
import { HeaderSearch } from "@dws-contributes/sitespro-gatsby-base/src/components/Organisms/Search"
import MainMenu from "@dws-contributes/sitespro-gatsby-base/src/components/Molecules/Menu/MainMenu/MainMenu"
import UtilityMenu from "@dws-contributes/sitespro-gatsby-base/src/components/Molecules/Menu/UtilityMenu/UtilityMenu"
import ActionMenu from "@dws-contributes/sitespro-gatsby-base/src/components/Molecules/Menu/ActionMenu/ActionMenu"
import Announcement from "@dws-contributes/sitespro-gatsby-base/src/components/Molecules/Announcement/Announcement"
import AlertBar from "@dws-contributes/sitespro-gatsby-base/src/components/Organisms/Header/AlertBar"
import Link from "@dws-contributes/sitespro-gatsby-base/src/components/Atoms/Link/Link"
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import Container from "react-bootstrap/Container"
import { siteDataShape } from "@dws-contributes/sitespro-gatsby-base/src/hooks/SiteInfoData"
import theme from "theme"
import styled from "styled-components"
import "./Header.scss"

// using custom logo if it exists, otherwise a standard Duke logo + site name
// For backwards compatibility (not all sites might have the updated theme.js), we'll set a fallback
const LogoImg = styled.img`
  height: ${props => props.height};
  width: ${props => props.width};
`

const Logo = ({ block = "logo", sitePrefix, siteName }) => {
  const logoImageSrc = theme.headerLogo.src
  const logoImageAlt = theme.headerLogo.alt

  if (logoImageSrc) {
    return (
      <LogoImg
        height={theme.headerLogo.height || "56px"}
        width={theme.headerLogo.width || "auto"}
        src={`/${logoImageSrc}`}
        alt={logoImageAlt}
        className="logo-image"
      />
    )
  } else {
    return (
      <div className="logo-wrapper">
        <div className={bem(block, "sitename", [])}>
          <div className={bem(block, "sitename", ["prefix"])}>
            {sitePrefix}
          </div>
          <div className={bem(block, "sitename", ["name"])}>{siteName}</div>
        </div>
      </div>
    )
  }
}

const Header = ({ siteData, block = "header" }) => {
  const { prefix, name, announcement } = siteData
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)
  const mobileMenuToggle = () => setIsMobileMenuOpen(!isMobileMenuOpen)

  return (
    <header className={theme.header}>
      {announcement && <Announcement announcement={announcement} />}
      <div className="header_wrapper">
        <Navbar expand="lg" variant="dark">
          <Navbar.Brand href="/" className="logo-mobile">
            <div className="logo-area-wrapper">
              <Logo sitePrefix={prefix} siteName={name} />
            </div>
          </Navbar.Brand>
          <Navbar.Toggle
            label="Toggle navigation menu"
            aria-controls="header-nav"
            aria-haspopup="true"
            onClick={mobileMenuToggle}
            aria-expanded={isMobileMenuOpen}
          >
            <span className="hamburger-box">
              <span className="hamburger-inner"></span>
            </span>
            <span className="navbar-hamburger-button_label label-menu">
              Menu
            </span>
            <span className="navbar-hamburger-button_label label-close">
              Close
            </span>
          </Navbar.Toggle>

          <Container>
            <Navbar.Collapse id="header-nav">
              <Nav className="mr-auto">
                <div className="utility_menu_wrapper">
                  <ActionMenu />
                  <UtilityMenu />
                </div>

                <div className="break"></div>

                <Navbar.Brand className="logo-desktop">
                  <div className="logo-area-wrapper">
                    <Link to="/">
                      <Logo sitePrefix={prefix} siteName={name} />
                    </Link>
                  </div>
                </Navbar.Brand>
                <HeaderSearch />

                <div className="header_menu_wrapper">
                  <MainMenu />
                </div>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
      <AlertBar />
    </header>
  )
}
Header.propTypes = {
  siteData: PropTypes.exact(siteDataShape),
  block: PropTypes.string,
}

export default Header
