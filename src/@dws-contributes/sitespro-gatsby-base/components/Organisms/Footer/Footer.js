import React from "react"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import PropTypes from "prop-types"
import Link from "@dws-contributes/sitespro-gatsby-base/src/components/Atoms/Link/Link"
import Heading from "@dws-contributes/sitespro-gatsby-base/src/components/Atoms/Heading/Heading"
import bem from "@dws-contributes/sitespro-gatsby-base/src/components/_utils/bem"
import FooterMenu from "@dws-contributes/sitespro-gatsby-base/src/components/Molecules/Menu/FooterMenu/FooterMenu"
import Button from "@dws-contributes/sitespro-gatsby-base/src/components/Atoms/Button/Button"
import { siteDataShape } from "@dws-contributes/sitespro-gatsby-base/src/hooks/SiteInfoData"
import DukeHealthLogo from "../../../images/DukeHealthLogo.svg"
import { FaFacebookF, FaTwitter, FaInstagram, FaYoutube, FaLinkedinIn } from "react-icons/fa"
import { MdPlace, MdLocalPhone } from "react-icons/md"
import { HiOutlineAtSymbol } from "react-icons/hi"
import theme from "theme"
import "./Footer.scss"

const Footer = ({ siteData, block = "footer" }) => {
  const {
    name,
    addressLines,
    email,
    phoneNumber,
    additionalInfo,
    primaryCTA,
    secondaryCTA,
    ctaHeading,
    ctaDescription,
    facebookUri,
    instagramUri,
    linkedinUri,
    twitterUri,
    youtubeUri,
  } = siteData

  const footerBackground = theme.footerBackground ? `url(/${theme.footerBackground}` : null
  return (
    <footer style={{ backgroundImage: footerBackground }}>
      <Container>
        <Row>
          <Col>
            <Link to="https://dukehealth.org">
              <img
                className={bem(block, "duke-svg", [])}
                src={DukeHealthLogo}
                alt="Duke Health"
              />
            </Link>
          </Col>
        </Row>
        <Row className="section-footer-menu">
          <Col md="4" lg="3" className="footer-contact">
            <h3 className="site_name">
              <Link to="/">
                {name}
              </Link>
            </h3>

            {addressLines.length > 0 && (
              <p className={bem(block, "address", [])}>
                <MdPlace />
                {addressLines.map(function (item, index) {
                  return <span key={`address_${index}`}>{item}</span>
                })}
              </p>
            )}

            {email && (
              <p className={bem(block, "email", [])}>
                <HiOutlineAtSymbol />
                <a href={`mailto:${email}`}>{email}</a>
              </p>
            )}

            {phoneNumber && (
              <p className={bem(block, "phone", [])}>
                <MdLocalPhone />
                <a href={`tel:${phoneNumber.replace(/[^\d]/g, '')}`}>{phoneNumber}</a>
              </p>
            )}

            {additionalInfo && <p>{additionalInfo}</p>}

            {/* Social icons */}
            <div
              className={`${bem(
                block,
                "social-icons-wrapper",
                []
              )}`}
            >
              {facebookUri && <a href={facebookUri} aria-label="Facebook link" title="Facebook link"><FaFacebookF /></a>}
              {twitterUri && <a href={twitterUri} aria-label="Twitter link" title="Twitter link"><FaTwitter /></a>}
              {instagramUri && <a href={instagramUri} aria-label="Instagram link" title="Instagram link"><FaInstagram /></a>}
              {youtubeUri && <a href={youtubeUri} aria-label="YouTube link" title="YouTube link"><FaYoutube /></a>}
              {linkedinUri && <a href={linkedinUri} aria-label="LinkedIn link" title="LinkedIn link"><FaLinkedinIn /></a>}
            </div>
          </Col>
          <Col md="4" lg="3">
            <nav className="nav footer-menu" aria-labelledby="footermenulabeldh1">
              <h2 id="footermenulabeldh1" className="visually-hidden">
                Footer navigation</h2>
              <ul>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/diversity-equity-and-inclusion/Duke-health-stands-against-racism" className="nav-link">Duke Health Anti-Racism Pledge</a>
                </li>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/notice-of-nondiscrimination" className="nav-link">Notice of Nondiscrimination</a>
                </li>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/privacy" className="nav-link">Notice of Privacy Practices</a>
                </li>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/terms-and-conditions-of-use" className="nav-link">Terms and Conditions</a>
                </li>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/privacy/website-privacy-policy" className="nav-link">Website Privacy Policy</a>
                </li>
              </ul>
            </nav>
          </Col>
          <Col md="4" lg="3">
            <nav className="nav footer-menu" aria-labelledby="footermenulabeldh2">
              <h2 id="footermenulabeldh2" className="visually-hidden">
                Footer navigation</h2>
              <ul>
                <li className="nav-item">
                  <a href="https://www.dukehealth.org/dukechildrens" className="nav-link">Duke Children's</a>
                </li>
                <li className="nav-item">
                  <a href="http://medschool.duke.edu/" className="nav-link">Duke School of Medicine</a>
                </li>
                <li className="nav-item">
                  <a href="http://nursing.duke.edu/" className="nav-link">Duke School of Nursing</a>
                </li>
                <li className="nav-item">
                  <a href="http://www.duke.edu/" className="nav-link">Duke University</a>
                </li>
              </ul>
            </nav>
          </Col>
          <Col  md="4" lg="3" className={bem(block, "right", [])}>
            <FooterMenu />

            {primaryCTA && (
              <Button link={primaryCTA.uri} showIcon={true} modifiers={["cta"]}>
                {primaryCTA.title}
              </Button>
            )}

            {/* Secondary CTA */}
            {ctaHeading && (
              <Heading level={4} className={bem(block, "cta-heading", [])}>
                {ctaHeading}
              </Heading>
            )}

            {ctaDescription && <p className="mb-0">{ctaDescription}</p>}

            {secondaryCTA && (
              <Button link={secondaryCTA.uri} showIcon={true} modifiers={["footer"]}>
                {secondaryCTA.title}
              </Button>
            )}
          </Col>
        </Row>
      </Container>
      <Container>
        <Row className="align-items-center">
          <Col md="6" className="copyrightWrapper">
            <p>Copyright &copy; 2004-{new Date().getFullYear()} Duke University Health System</p>
          </Col>
          <Col md="6">
            <div className="footer-bottom-menu">
              <a href="https://oarc.duke.edu/privacy/duke-university-privacy-statement">Privacy Statement</a>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  )
}
Footer.propTypes = {
  siteData: PropTypes.exact(siteDataShape),
  block: PropTypes.string,
}

export default Footer
