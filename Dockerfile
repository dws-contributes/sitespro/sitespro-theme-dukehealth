FROM image-mirror-prod-registry.cloud.duke.edu/library/node:18

LABEL maintainer "DWS <dws-oit@duke.edu>"
LABEL name 'duke-nodejs-18-container'
LABEL version '0.1'
LABEL release '1'
LABEL vendor 'Duke University, Office of Information Technology, Automation'
LABEL summary 'Nodejs container containing a built out gatsbyjs application'

# Development Port
EXPOSE 8080
# Production port
EXPOSE 8000

COPY . /usr/src/app

WORKDIR /usr/src/app

RUN chown -R 1000:0 /usr/src/app && \
    mkdir /.npm && \
    chown -R 1000:0 /.npm && \
    chmod ug+w -R /usr/src/app && \
    npm config set prefix /.npm && \
    export PATH="$PATH:/.npm/bin"

USER 1000

RUN npm install --no-optional --no-audit

CMD node_modules/.bin/gatsby develop -H 0.0.0.0 --verbose --no-color
