module.exports = {
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `${__dirname}/src/images/favicons/icon-duke-health-152x152.png`,
        icons: [
          {
            src: "favicons/icon-duke-health-72x72.png",
            sizes: "72x72",
            type: "image/png",
          },
          {
            src: "favicons/icon-duke-health-76x76.png",
            sizes: "76x76",
            type: "image/png",
          },
          {
            src: "favicons/icon-duke-health-114x114.png",
            sizes: "114x114",
            type: "image/png",
          },
          {
            src: "favicons/icon-duke-health-120x120.png",
            sizes: "120x120",
            type: "image/png",
          },
          {
            src: "favicons/icon-duke-health-144x144.png",
            sizes: "144x144",
            type: "image/png",
          },
          {
            src: "favicons/icon-duke-health-152x152.png",
            sizes: "152x152",
            type: "image/png",
          },
        ],
      },
    },
  ],
};
